// fetch('https://jsonplaceholder.typicode.com/todos')
//   .then((response) => response.json())
//   .then((data) => console.log(data))

// fetch('https://jsonplaceholder.typicode.com/todos/1')
//   .then((response) => response.json())
//   .then((data) =>
//     console.log(`The item '${data.title}' has a status of ${data.completed}`)
//   )

// fetch('https://jsonplaceholder.typicode.com/todos')
//   .then((response) => response.json())
//   .then((data) => {
//     let list = data.map((todo) => {
//       return todo.title
//     })
//     console.log(list)
//   })

// fetch('https://jsonplaceholder.typicode.com/todos', {
//   method: 'POST',
//   headers: { 'Content-type': 'application/json' },
//   body: JSON.stringify({
//     title: 'Test'
//   })
//   let list = data.map((todo) => {
//     return
//   })
// })
// .then((response) => response.json())
// .then((data) => console.log(data))

// fetch('https://jsonplaceholder.typicode.com/todos', {
//   method: 'POST',
//   headers: { 'Content-type': 'application/json' },
//   body: JSON.stringify({
//     title: 'Post todo',
//     completed: false,
//     userId: 1
//   })
// })
//   .then((response) => response.json())
//   .then((data) => console.log(data))

// Put
// fetch('https://jsonplaceholder.typicode.com/todos/69', {
//   method: 'PUT',
//   headers: { 'Content-type': 'application/json' },
//   body: JSON.stringify({
//     title: 'Updated To Do List Item',
//     description: 'To update the my to do list with a different data structure.',
//     status: 'Pending',
//     dateCompleted: 'Pending',
//     userId: 1
//   })
// })
//   .then((response) => response.json())
//   .then((data) => console.log(data))

// Patch
// fetch('https://jsonplaceholder.typicode.com/todos/69', {
//   method: 'PATCH',
//   headers: { 'Content-type': 'application/json' },
//   body: JSON.stringify({
//     status: 'Complete',
//     dateCompleted: '01/19/22'
//   })
// })
//   .then((response) => response.json())
//   .then((data) => console.log(data))

// Deleting a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/69', {
  method: 'DELETE'
})
  .then((response) => response.json())
  .then((data) => console.log('data'))
