console.log(
  fetch('https://jsonplaceholder.typicode.com/posts')
    .then((response) => response.json())
    .then((data) => console.log(data))
)

fetch('https://jsonplaceholder.typicode.com/posts', {
  method: 'POST',
  headers: {
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    title: 'New Post',
    body: 'Hello World',
    userID: 1
  })
})
  .then((response) => response.json())
  .then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PUT',
  headers: {
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    id: 1,
    title: 'Updated post',
    body: 'Hello Again',
    userID: 1
  })
})
  .then((response) => response.json())
  .then((data) => console.log(data))
fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'PATCH',
  headers: {
    'Content-type': 'application/json'
  },
  body: JSON.stringify({
    title: 'Correct Title'
  })
})
  .then((response) => response.json())
  .then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/posts/1', {
  method: 'DELETE'
})
  .then((response) => response.json())
  .then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/posts/1')
  .then((response) => response.json())
  .then((data) => console.log(data))
